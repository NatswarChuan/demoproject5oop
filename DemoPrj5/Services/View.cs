﻿using DemoPrj5.Entities;
using System;
using System.Collections.Generic;

namespace DemoPrj5.Services
{
    public static class View
    {
        const int LENGHT = 25;

        /// <summary>
        /// màn hình đăng nhập
        /// </summary>
        public static void Login()
        {
            Function.GetAllData();
            User user = null;
            int count = 0;
            List<IKeyValue> keyValuesInput = new List<IKeyValue>()
            {
                new KeyValue<string>
                    { Key = "User", Value = string.Empty },
                new KeyValue<string>
                    { Key = "Password", Value = string.Empty, IsPassword = true }
            };

            do
            {
                if (++count == 3)
                {
                    break;
                }
                HeaderTitle("DANG NHAP HE THONG");
                Input(keyValuesInput);
                Console.WriteLine();
                if ((user = User.FindByName(((KeyValue<string>)keyValuesInput[0]).Value)) == null || !user.CheckPassword(((KeyValue<string>)keyValuesInput[1]).Value))
                {
                    Warning("Tai khoan hoac mat khau khong dung!");
                }
                else
                {
                    break;
                }
            } while (true);

            if (user != null)
            {
                Menu();
            }
        }

        /// <summary>
        /// màn hình menu
        /// </summary>
        private static void Menu()
        {
            List<IKeyValue> keyValueSelects = new List<IKeyValue>()
            {
                new KeyValue<string>
                    { Key = "1", Value = "Xu ly đat ve" },
                new KeyValue<string>
                    { Key = "2", Value = "Xu ly tra ve" },
                new KeyValue<string>
                    { Key = "3", Value = "Thong ke" },
                new KeyValue<string>
                    { Key = "ESC", Value = "Thoat chuong trinh" }
            };
            List<IKeyValue> keyValueInputs = new List<IKeyValue>()
            {
                new KeyValue<char>
                    { Key = "Lua chon", Value = char.MinValue }
            };

            do
            {
                HeaderTitle("DANH MUC HE THONG");
                SelectItemMenu(keyValueSelects);
                Input(keyValueInputs);
            } while (((KeyValue<char>)keyValueInputs[0]).Value != '1' 
            && ((KeyValue<char>)keyValueInputs[0]).Value != '2' 
            && ((KeyValue<char>)keyValueInputs[0]).Value != '3' 
            && ((KeyValue<char>)keyValueInputs[0]).Value != (char)ConsoleKey.Escape);

            switch (((KeyValue<char>)keyValueInputs[0]).Value)
            {
                case '1':
                    Booking();
                    break;
                case '2':
                    ReturnTicket();
                    break;
                case '3':
                    Statistical();
                    break;
                default:
                    Function.SaveAllData();
                    break;
            }
        }

        /// <summary>
        /// màn hình đặt vé
        /// </summary>
        private static void Booking()
        {
            List<IKeyValue> keyValueSelects = new List<IKeyValue>()
            {
                new KeyValue<string>
                    { Key = "1", Value = "Khach hang moi" },
                new KeyValue<string>
                    { Key = "2", Value = "Khach hang cu" },
                new KeyValue<string>
                    { Key = "3", Value = "Quay ve" },
                new KeyValue<string>
                    { Key = "ESC", Value = "Thoat chuong trinh" }
            };
            List<IKeyValue> keyValueInputs = new List<IKeyValue>()
            {
                new KeyValue<char>
                    { Key = "Lua chon", Value = char.MinValue }
            };
            do
            {
                HeaderTitle("XU LY DAT VE");
                SelectItemMenu(keyValueSelects);
                Input(keyValueInputs);
            } while (((KeyValue<char>)keyValueInputs[0]).Value != '1'
            && ((KeyValue<char>)keyValueInputs[0]).Value != '2'
            && ((KeyValue<char>)keyValueInputs[0]).Value != '3'
            && ((KeyValue<char>)keyValueInputs[0]).Value != (char)ConsoleKey.Escape);

            switch (((KeyValue<char>)keyValueInputs[0]).Value)
            {
                case '1':
                    BookingByCustommer();
                    break;
                case '2':
                    BookingById();
                    break;
                case '3':
                    Menu();
                    break;
            }
        }

        /// <summary>
        /// màn hình đặt vé theo id khách hàng
        /// </summary>
        private static void BookingById()
        {
            List<IKeyValue> keyValuesInput = new List<IKeyValue>()
            {
                new KeyValue<int>
                    { Key = "Ma chuyen bay", Value = 0 },
                new KeyValue<int>
                    { Key = "Ma khach hang", Value = 0},
                new KeyValue<int>
                    { Key = "Ma ghe", Value = 0}
            };
            do
            {
                HeaderTitle("XU LY DAT VE");
                Input(keyValuesInput);
                try
                {
                    _ = new Ticket(
                        flightId: ((KeyValue<int>)keyValuesInput[0]).Value,
                        custommerId: ((KeyValue<int>)keyValuesInput[1]).Value, 
                        chair: ((KeyValue<int>)keyValuesInput[2]).Value);
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                }
            } while (true);
            Menu();
        }

        /// <summary>
        /// màn hình đặt vé cho khách hàng mới
        /// </summary>
        private static void BookingByCustommer()
        {
            Custommer custommer = null;
            List<IKeyValue> keyValuesInput = new List<IKeyValue>()
            {
                new KeyValue<int>
                    { Key = "Ma chuyen bay", Value = 0 },
                new KeyValue<string>
                    { Key = "Ten khach hang", Value = string.Empty},
                new KeyValue<string>
                    { Key = "CMND khach hang", Value = string.Empty},
                new KeyValue<int>
                    { Key = "Ma ghe", Value = 0}
            };
            do
            {
                HeaderTitle("XU LY DAT VE");
                Input(keyValuesInput);
                try
                {
                    custommer = new Custommer(
                        identityCard: ((KeyValue<string>)keyValuesInput[2]).Value, 
                        name: ((KeyValue<string>)keyValuesInput[1]).Value);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                    continue;
                }
                try
                {
                    _ = new Ticket(flightId: ((KeyValue<int>)keyValuesInput[0]).Value, 
                        custommerId: custommer.Id, 
                        chair: ((KeyValue<int>)keyValuesInput[3]).Value);
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                }
            } while (true);
            Menu();
        }

        /// <summary>
        /// màn hình hoàn vé
        /// </summary>
        private static void ReturnTicket()
        {
            List<IKeyValue> keyValuesInput = new List<IKeyValue>()
            {
                new KeyValue<string>
                    { Key = "Ma ve", Value = string.Empty}
            };
            do
            {
                HeaderTitle("XU LY TRA VE");
                Input(keyValuesInput);
                if (Ticket.ReturnTicketById(((KeyValue<string>)keyValuesInput[0]).Value))
                {
                    break;
                }
                Warning("Ma the khong ton tai");
            } while (true);
            Menu();
        }

        /// <summary>
        /// màn hình thống kê
        /// </summary>
        private static void Statistical()
        {
            List<IKeyValue> keyValueSelects = new List<IKeyValue>()
            {
                new KeyValue<string>
                    { Key = "1", Value = "Danh sach khach hang tren chuyen bay" },
                new KeyValue<string>
                    { Key = "2", Value = "Danh sach ghe con trong tren chuyen bay" },
                new KeyValue<string>
                    { Key = "3", Value = "So luong chuyen bay cua may bay" },
                new KeyValue<string>
                    { Key = "4", Value = "Quay ve" },
                new KeyValue<string>
                    { Key = "ESC", Value = "Thoat chuong trinh" }
            };
            List<IKeyValue> keyValueInputs = new List<IKeyValue>()
            {
                new KeyValue<char>
                    { Key = "Lua chon", Value = char.MinValue }
            };
            do
            {
                HeaderTitle("THONG KE");
                SelectItemMenu(keyValueSelects);
                Input(keyValueInputs);
            } while (((KeyValue<char>)keyValueInputs[0]).Value != '1' 
            && ((KeyValue<char>)keyValueInputs[0]).Value != '2' 
            && ((KeyValue<char>)keyValueInputs[0]).Value != '3' 
            && ((KeyValue<char>)keyValueInputs[0]).Value != '4'
            && ((KeyValue<char>)keyValueInputs[0]).Value != (char)ConsoleKey.Escape);

            switch (((KeyValue<char>)keyValueInputs[0]).Value)
            {
                case '1':
                    ShowCustommersInFlight();
                    break;
                case '2':
                    ShowEmptyChairInFlight();
                    break;
                case '3':
                    ShowFlightOfPlanes();
                    break;
                case '4':
                    Menu();
                    break;
            }
        }

        /// <summary>
        /// màn hình hiển thị danh sách khách hàng trên chuyến bay
        /// </summary>
        private static void ShowCustommersInFlight()
        {
            Flight flight = null;
            List<IKeyValue> keyValueInputs = new List<IKeyValue>()
            {
                new KeyValue<int>
                    { Key = "Ma chuyen bay", Value = int.MinValue }
            };
            do
            {
                HeaderTitle("DANH SACH GHE TRONG TREN CHUYEN BAY");
                Input(keyValueInputs);
                flight = Flight.FindById(((KeyValue<int>)keyValueInputs[0]).Value);
                if (flight != null)
                {
                    flight.ShowCustommerList();
                    Console.ReadKey();
                    break;
                }
                Warning("Khong co chuyen bay");
            } while (flight == null);
            Menu();
        }

        /// <summary>
        /// màn hình hiển thị các ghế còn trống
        /// </summary>
        private static void ShowEmptyChairInFlight()
        {
            Flight flight = null;
            List<IKeyValue> keyValueInputs = new List<IKeyValue>()
            {
                new KeyValue<int>
                    { Key = "Ma chuyen bay", Value = int.MinValue }
            };
            do
            {
                HeaderTitle("DANH SACH KHACH HANG TREN CHUYEN BAY");
                Input(keyValueInputs);
                flight = Flight.FindById(((KeyValue<int>)keyValueInputs[0]).Value);
                if (flight != null)
                {
                    flight.ShowEmptyChairList();
                    Console.ReadKey();
                    break;
                }
                Warning("Khong co chuyen bay");
            } while (flight == null);
            Menu();
        }

        /// <summary>
        /// màn hình hiển thị các chuyến của máy bay
        /// </summary>
        private static void ShowFlightOfPlanes()
        {
            Planes planes = null;
            List<IKeyValue> keyValueInputs = new List<IKeyValue>()
            {
                new KeyValue<int>
                    { Key = "Ma may bay", Value = int.MinValue }
            };
            do
            {
                HeaderTitle("THONG KE CAC CHUYEN BAY CUA MAY BAY");
                Input(keyValueInputs);
                planes = Planes.FindById(((KeyValue<int>)keyValueInputs[0]).Value);
                if (planes != null)
                {
                    planes.ShowFlightsList();
                    Console.ReadKey();
                    break;
                }
                Warning("Khong co may bay");
            } while (planes == null);
            Menu();
        }

        /// <summary>
        /// Hiển thị tiêu đề
        /// </summary>
        /// <param name="title">tên tiêu đề</param>
        public static void HeaderTitle(string title)
        {
            title = title.ToUpper();
            int titleLength = title.Length;
            int lenght = ((LENGHT) * 2 - 2 - 1 - titleLength) / 2;
            bool checkLength = (LENGHT * 2 - 1) == (titleLength + (lenght * 2) + 2);
            Console.Clear();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("\t");
            for (int i = 0; i < LENGHT; i++)
            {
                Console.Write("* ");
            }
            Console.WriteLine();
            Console.Write("\t");
            Console.Write("*" + Function.GenStr(lenght, " "));
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(title);
            Console.ForegroundColor = ConsoleColor.Yellow;
            if (checkLength)
            {
                Console.WriteLine(Function.GenStr(lenght, " ") + "*");
            }
            else
            {
                Console.WriteLine(Function.GenStr(lenght + 1, " ") + "*");
            }

            Console.Write("\t");
            for (int i = 0; i < LENGHT; i++)
            {
                Console.Write("* ");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// nhập dữ liệu
        /// </summary>
        /// <param name="data">dữ liệu cần nhập
        ///     <param name="Key">
        ///         câu xuất ra màn hình
        ///     </param>
        ///     <param name="Value">
        ///         dữ liệu cần nhập
        ///     </param>
        /// </param>
        public static void Input(List<IKeyValue> data)
        {
            int maxLengthKey = GetMaxLength(data);

            int lenght = LENGHT - maxLengthKey - 2;
            int itemLenght = 0;

            foreach (var dataItem in data)
            {
                itemLenght = dataItem.GetKey().Length;
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write($"\t{Function.GenStr(lenght, " ")}{dataItem.GetKey()}");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write($"{Function.GenStr(maxLengthKey - itemLenght, " ")} : ");

                switch(dataItem.GetValueType())
                {
                    case "Int32":
                        int value;
                        int.TryParse(Console.ReadLine(), out value);
                        ((KeyValue<int>)dataItem).Value = value;
                        break;

                    case "String":
                        if (((KeyValue<string>)dataItem).IsPassword)
                        {
                            ((KeyValue<string>)dataItem).Value = Function.InputPassword();
                        }
                        else
                        {
                            ((KeyValue<string>)dataItem).Value = Console.ReadLine();
                        }
                        break;

                    case "Char":
                        ((KeyValue<char>)dataItem).Value = Console.ReadKey().KeyChar;
                        break;
                }
            }
        }

        /// <summary>
        /// hiển thị menu
        /// </summary>
        /// <param name="data">dữ liệu cần nhập
        ///     <param name="Key">
        ///         lựa chọn
        ///     </param>
        ///     <param name="Value">
        ///         thực hiện
        ///     </param>
        /// </param>
        public static void SelectItemMenu(List<IKeyValue> data)
        {
            int maxLengthKey = GetMaxLength(data);

            int lenght = LENGHT - maxLengthKey - 2;
            int itemLenght = 0;

            foreach (var dataItem in data)
            {
                itemLenght = dataItem.GetKey().Length;
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write($"\t{Function.GenStr(lenght, " ")}{dataItem.GetKey()}");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"{Function.GenStr(maxLengthKey - itemLenght, " ")} : {((KeyValue<string>)dataItem).Value}");
            }
        }

        /// <summary>
        /// lây độ dài tối đa của key
        /// </summary>
        /// <param name="data">mảng KeyValue</param>
        /// <returns></returns>
        public static int GetMaxLength(List<IKeyValue> data)
        {
            int maxLength = int.MinValue;

            foreach (var item in data)
            {
                int itemLenght = item.GetKey().Length;
                if (itemLenght > maxLength)
                {
                    maxLength = itemLenght;
                }
            }

            return maxLength;
        }

        /// <summary>
        /// cảnh báo
        /// </summary>
        /// <param name="str">chuỗi hiển thị</param>
        public static void Warning(string str)
        {
            int lenght = LENGHT - (str.Length/2) - 1;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"\t{Function.GenStr(lenght," ")}{str}");
            Console.ReadKey();
        }
    }
}
