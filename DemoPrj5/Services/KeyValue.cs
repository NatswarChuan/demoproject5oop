﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Services
{
    public interface IKeyValue
    {
        public string GetKey();
        public string GetValueType();
    }
    public class KeyValue<T> : IKeyValue
    {
        public string Key { get; set; }
        public T Value { get; set; }
        public bool IsPassword { get; set; }

        public string GetKey()
        {
            return Key;
        }

        public string GetValueType()
        {
            return Value.GetType().Name;
        }
    }
}
