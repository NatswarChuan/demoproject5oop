﻿using DemoPrj5.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Services
{
    public static class Function
    {
        /// <summary>
        /// Lấy tất cả dữ liệu
        /// </summary>
        public static void GetAllData()
        {
            User.GetObjectsFromFile();
            Airport.GetObjectsFromFile();
            Custommer.GetObjectsFromFile();
            Planes.GetObjectsFromFile();
            Flight.GetObjectsFromFile();
            Ticket.GetObjectsFromFile();
        }

        /// <summary>
        /// lưu tất cả dữ liệu
        /// </summary>
        public static void SaveAllData()
        {
            User.SaveObjectsToFile();
            Airport.SaveObjectsToFile();
            Custommer.SaveObjectsToFile();
            Planes.SaveObjectsToFile();
            Flight.SaveObjectsToFile();
            Ticket.SaveObjectsToFile();
        }


        /// <summary>
        /// chức năng nhập mật khẩu
        /// </summary>
        /// <returns></returns>
        public static string InputPassword()
        {
            var pass = string.Empty;
            ConsoleKey key;
            do
            {
                var keyInfo = Console.ReadKey(intercept: true);
                key = keyInfo.Key;

                if (key == ConsoleKey.Backspace && pass.Length > 0)
                {
                    Console.Write("\b \b");
                    pass = pass[0..^1];
                }
                else if (!char.IsControl(keyInfo.KeyChar))
                {
                    Console.Write("*");
                    pass += keyInfo.KeyChar;
                }
            } while (key != ConsoleKey.Enter);
            return pass;
        }

        /// <summary>
        /// tự động tạo chuỗi theo số lượng
        /// </summary>
        /// <param name="n">số lượng</param>
        /// <param name="str">chuỗi</param>
        /// <returns>chuỗi đã tạo</returns>
        public static string GenStr(int n,string str)
        {
            string strs = "";
            for (int i = 0; i < n; i++)
            {
                strs += str;
            }
            return strs;
        }
    }
}
