﻿/**
 * San bay
 * */
using DemoPrj5.Interfaces;
using DemoPrj5.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Entities
{
    public class Airport
    {
        const string FILE = "DB/AirportsList.txt";
        private static List<Airport> _airportsList;
        private static int _autoIncrement = 0;

        private int _id;
        private string _name;

        public static List<Airport> AirportsList { get => _airportsList;}
        public int Id { get => _id;  }
        public string Name { get => _name; set => _name = value; }

        /// <summary>
        /// Hàm tạo sân bay
        /// </summary>
        /// <param name="name">tên sân bay</param>
        /// <param name="id">id sân bay</param>
        /// <exception cref="Exception">dữ liệu lỗi</exception>
        public Airport(string name = "",int? id = null)
        {
            if (name == "")
            {
                throw new Exception("Ten rong");
            }
            if (_airportsList == null)
            {
                _airportsList = new List<Airport>();
            }
            this._name = name;
            this._id = id.HasValue? id.Value:++_autoIncrement;
            _airportsList.Add(this);
        }

        /// <summary>
        /// tìm sân bay theo id
        /// </summary>
        /// <param name="id">id sân bay</param>
        /// <returns>sân bay || null</returns>
        public static Airport FindById(int id)
        {
            Airport airport = null;

            if (_airportsList != null)
            {
                foreach (Airport airportItem in _airportsList)
                {
                    if (airportItem.Id == id)
                    {
                        airport = airportItem;
                        break;
                    }
                }
            }

            return airport;
        }

        /// <summary>
        /// định dạng chuỗi lưu vào file
        /// </summary>
        /// <returns>mảng dữ liệu lưu file</returns>
        public static string[] ToFile()
        {
            List<string> strs = new List<string>();
            strs.Add($"{_autoIncrement}");
            if (_airportsList != null)
            {
                foreach (Airport airportItem in _airportsList)
                {
                    strs.Add($"{airportItem.Id}-{airportItem.Name}");
                }
            }
            return strs.ToArray();
        }

        /// <summary>
        /// đọc dữ liệu file
        /// </summary>
        public static void GetObjectsFromFile()
        {
            List<string> airportsListData = IOFIle.ReadFile(FILE).ToList();
            if (airportsListData.Count > 0)
            {
                for (int i = 0; i < airportsListData.Count; i++)
                {
                    if (i == 0)
                    {
                        _autoIncrement = int.Parse(airportsListData[i]);
                    }
                    else
                    {
                        string[] userData = airportsListData[i].Split("-");
                        try
                        {
                            _ = new Airport(id: int.Parse(userData[0]), name: userData[1]);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// lưu dữ liệu vào file
        /// </summary>
        public static void SaveObjectsToFile()
        {
            if (!Directory.Exists("DB"))
            {
                Directory.CreateDirectory("DB");
            }
            IOFIle.WriteFile(FILE, ToFile());
        }

        public override string ToString() => $"Airport:\tId: {this.Id}\tName: {this.Name}";

    }
}
