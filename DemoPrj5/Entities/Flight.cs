﻿/**
 * Chuyen bay
 * */
using DemoPrj5.Interfaces;
using DemoPrj5.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Entities
{
    public class Flight : IFlight
    {
        const string FILE = "DB/FlightsList.txt";
        private static Dictionary<int, string> _statusDic;
        private static List<Flight> _flightsList;
        private static int _autoIncrement = 0;

        private int _id;
        private Planes _planes;
        private DateTime _departureDay;
        private List<Ticket> _ticketList;
        private List<int> _chairsList;
        private int _status;
        private Airport _airport;
        private List<Custommer> _custommersList;

        public static List<Flight> FlightsList { get => _flightsList; }
        public static Dictionary<int, string> StatusDic { get => _statusDic; }
        public int Id { get => _id; }
        public Planes @Planes { get => _planes; }
        public DateTime DepartureDay { get => _departureDay; }
        public List<Ticket> TicketList { get => _ticketList; }
        public List<Custommer> CustommersList { get => _custommersList; }
        public List<int> ChairsList { get => _chairsList; }
        public int Status { get => _status; set => _status = value; }
        public Airport Airport { get => _airport; }

        /// <summary>
        /// Hàm tạo chuyến bay
        /// </summary>
        /// <param name="planesId">id máy bay</param>
        /// <param name="departureDay">ngày bay</param>
        /// <param name="status">trạng thái</param>
        /// <param name="airportId">id sân bay</param>
        /// <param name="id">id chuyến bayy</param>
        /// <exception cref="Exception">lỗi dữ liệu không hợp lệ</exception>
        public Flight(int planesId = 0, string departureDay = "", int status = -1, int airportId = 0, int? id = null)
        {
            Planes planes = Planes.FindById(planesId);
            if (planes == null)
            {
                throw new Exception("Khong tim thay may bay");
            }
            Airport airport = Airport.FindById(airportId);
            if (airport == null)
            {
                throw new Exception("san bay khong hop le");
            }

            if (status < 0 || status > 3)
            {
                throw new Exception("Trang thai khong hop le");
            }

            try
            {
                this._departureDay = Convert.ToDateTime(departureDay);
            }
            catch (Exception)
            {
                throw;
            }

            if (_flightsList == null)
            {
                _flightsList = new List<Flight>();
            }

            if (_statusDic == null)
            {
                _statusDic = new Dictionary<int, string>();
                _statusDic.Add(0, "hủy chuyến");
                _statusDic.Add(1, "còn vé");
                _statusDic.Add(2, "hết vé");
                _statusDic.Add(3, "hoàn tất");
            }

            this._planes = planes;
            this._status = status;
            this._id = id.HasValue ? id.Value : ++_autoIncrement;
            this._ticketList = new List<Ticket>();
            this._chairsList = GenChairsList();
            this._custommersList = new List<Custommer>();
            this._airport = airport;

            this.Planes.Flights.Add(this);
            FlightsList.Add(this);
        }

        /// <summary>
        /// tìm kiếm chuyến bay theo id
        /// </summary>
        /// <param name="id">id chuyến bay</param>
        /// <returns>chuyến bay || null</returns>
        public static Flight FindById(int id)
        {
            Flight flight = null;

            if (_flightsList != null)
            {
                foreach (Flight flightItem in FlightsList)
                {
                    if (flightItem.Id == id)
                    {
                        flight = flightItem;
                        break;
                    }
                }
            }

            return flight;
        }


        /// <summary>
        /// định dạng chuỗi lưu vào file
        /// </summary>
        /// <returns>mảng dữ liệu lưu file</returns>
        public static string[] ToFile()
        {
            List<string> strs = new List<string>();
            strs.Add($"{_autoIncrement}");
            if (FlightsList != null)
            {
                foreach (Flight flightsItem in FlightsList)
                {
                    strs.Add($"{flightsItem.Id}-{flightsItem._planes.Id}-{flightsItem.DepartureDay.ToShortDateString()}-{flightsItem.Status}-{flightsItem.Airport.Id}");
                }
            }
            return strs.ToArray();
        }

        /// <summary>
        /// đọc dữ liệu file
        /// </summary>
        public static void GetObjectsFromFile()
        {
            List<string> flightsListData
                = IOFIle.ReadFile(FILE).ToList();
            if (flightsListData.Count > 0)
            {
                for (int i = 0; i < flightsListData.Count; i++)
                {
                    if (i == 0)
                    {
                        _autoIncrement = int.Parse(flightsListData[i]);
                    }
                    else
                    {
                        string[] flightsData = flightsListData[i].Split("-");
                        try
                        {
                            _ = new Flight(id: int.Parse(flightsData[0]), planesId: int.Parse(flightsData[1]), departureDay: flightsData[2], status: int.Parse(flightsData[3]), airportId: int.Parse(flightsData[4]));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// lưu dữ liệu vào file
        /// </summary>
        public static void SaveObjectsToFile()
        {
            if (!Directory.Exists("DB"))
            {
                Directory.CreateDirectory("DB");
            }
            IOFIle.WriteFile(FILE, ToFile());
        }

        /// <summary>
        /// tự động tạo danh sách ghế ngồi theo số ghế của máy bay
        /// </summary>
        /// <returns>danh sách ghế</returns>
        public List<int> GenChairsList()
        {
            List<int> list = null;
            if (this._planes != null)
            {
                list = new List<int>();
                for (int i = 1; i < this._planes.Seats; i++)
                {
                    list.Add(i);
                }
            }
            return list;
        }

        /// <summary>
        /// chuyển danh sách vé sang chuỗi 
        /// </summary>
        /// <returns></returns>
        public string TicketListToString()
        {
            string str = "";
            foreach (Ticket ticketItem in this._ticketList)
            {
                str += ticketItem + "\t";
            }
            return str;
        }

        /// <summary>
        /// hiển thị danh sách ghê trống
        /// </summary>
        public void ShowEmptyChairList()
        {
            for (int i = 1; i <= this._chairsList.Count; i++)
            {
                Console.ForegroundColor = i%2 == 0? ConsoleColor.Green : ConsoleColor.Yellow;
                Console.Write($"{_chairsList[i - 1]}\t");
                if (i % 7 == 0)
                {
                    Console.WriteLine();
                }
            }
        }

        /// <summary>
        /// hiển thị danh sách khách hàng của 1 chuyến 
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void ShowCustommerList()
        {
            foreach (Ticket ticketItem in this._ticketList)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("\t   Ma ve");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($": {ticketItem.Id}");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("\t\tTen khach hang");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($": {ticketItem.Custommer.Name}");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("\t\tSo ghe");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($": {ticketItem.Chair}");
            }
        }

        /// <summary>
        /// tìm kiếm khách hang theo tên
        /// </summary>
        /// <param name="name">tên khách hàng</param>
        /// <returns></returns>
        public List<Custommer> FindCustommerByName(string name)
        {
            List<Custommer> custommers = new List<Custommer>();
            foreach (Custommer custommerItem in this._custommersList)
            {
                if (custommerItem.Name.Contains(name))
                {
                    custommers.Add(custommerItem);
                }
            }
            return custommers;
        }

        /// <summary>
        /// kiểm tra ghế trống
        /// </summary>
        /// <param name="chair">ghế</param>
        /// <returns>trống || đã có người ngồi</returns>
        public bool CheckedChair(int chair)
        {
            bool result = false;

            if (this._chairsList != null)
            {
                foreach (int chairItem in this._chairsList)
                {
                    if (chairItem == chair)
                    {
                        result = true;
                        break;
                    }
                }
            }

            return result;
        }

        public override string ToString() => $"Mã chuyến bay: {this.Id}\tNgày khởi hành: {this.DepartureDay.ToShortDateString()}\t" +
            $"Sân bay đến: {this.Airport.Name}\tTrạng thái: {_statusDic.GetValueOrDefault(this.Status)}\tDanh sách vé: {TicketListToString()}";


    }
}
