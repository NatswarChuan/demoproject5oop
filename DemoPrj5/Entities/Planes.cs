﻿/**
 * May bay
 * */
using DemoPrj5.Interfaces;
using DemoPrj5.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Entities
{
    public class Planes : Iplanes
    {
        const string FILE = "DB/PlanesList.txt";
        private static List<Iplanes> _planesList;
        private static int _autoIncrement = 0;

        private int _id;
        private int _seats;
        private List<Flight> _flights;

        public static List<Iplanes> PlanesList { get => _planesList; }
        public int Id { get => _id; }
        public int Seats { get => _seats; set => _seats = value; }
        public List<Flight> Flights { get => _flights; }

        /// <summary>
        /// hàm tại máy bay
        /// </summary>
        /// <param name="seats">số lượng ghế</param>
        /// <param name="id">id máy bay</param>
        public Planes(int seats = 0, int? id = null)
        {
            if (_planesList == null)
            {
                _planesList = new List<Iplanes>();
            }
            this._flights = new List<Flight>();
            this._seats = seats;
            this._id = id.HasValue ? id.Value : ++_autoIncrement;
            _planesList.Add(this);
        }

        /// <summary>
        /// tìm kuếm máy bay theo id
        /// </summary>
        /// <param name="id">id máy bay</param>
        /// <returns>máy bay || null</returns>
        public static Planes FindById(int id)
        {
            Planes planes = null;

            if (_planesList != null)
            {
                foreach (Planes planesItem in _planesList)
                {
                    if (planesItem.Id == id)
                    {
                        planes = planesItem;
                        break;
                    }
                }
            }

            return planes;
        }

        /// <summary>
        /// hiển thị danh sách chuyến bay của máy bay
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void ShowFlightsList()
        {

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write("\t   Tong so chuyen bay");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($": {this._flights.Count}");
            foreach (Flight flightItem in this._flights)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("\t   Ma chuyen bay");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($": {flightItem.Id}");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("\t\tNgay bay");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($": {flightItem.DepartureDay.ToShortDateString()}");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("\t\tSan bay den");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($": {flightItem.Airport.Name}");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("\t\tTrang thai");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($": {Flight.StatusDic.GetValueOrDefault(flightItem.Status)}");
            }
        }

        /// <summary>
        /// định dạng chuỗi lưu vào file
        /// </summary>
        /// <returns>mảng dữ liệu lưu file</returns>
        public static string[] ToFile()
        {
            List<string> strs = new List<string>();
            strs.Add($"{_autoIncrement}");
            if (_planesList != null)
            {
                foreach (Planes planesItem in _planesList)
                {
                    strs.Add($"{planesItem.Id}-{planesItem.Seats}");
                }
            }
            return strs.ToArray();
        }

        /// <summary>
        /// đọc dữ liệu file
        /// </summary>
        public static void GetObjectsFromFile()
        {
            List<string> planesListData
                = IOFIle.ReadFile(FILE).ToList();
            if (planesListData.Count > 0)
            {
                for (int i = 0; i < planesListData.Count; i++)
                {
                    if (i == 0)
                    {
                        _autoIncrement = int.Parse(planesListData[i]);
                    }
                    else
                    {
                        string[] planesData = planesListData[i].Split("-");
                        try
                        {
                            _ = new Planes(id: int.Parse(planesData[0]), seats: int.Parse(planesData[1]));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// lưu dữ liệu vào file
        /// </summary>
        public static void SaveObjectsToFile()
        {
            if (!Directory.Exists("DB"))
            {
                Directory.CreateDirectory("DB");
            }
            IOFIle.WriteFile(FILE, ToFile());
        }

        public override string ToString() => $"Planes:\n\tId: {this._id}\n\tSeats: {this._seats}";

    }
}
