﻿/**
 * Ve 
 * */
using DemoPrj5.Interfaces;
using DemoPrj5.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Entities
{
    public class Ticket : ITicket
    {
        const string FILE = "DB/TicketsList.txt";
        private static List<Ticket> _ticketsList;

        private string _id;
        private Flight _flight;
        private Custommer _custommer;
        private int _chair;

        public static List<Ticket> TicketsList { get => _ticketsList; }
        public string Id { get => _id; }
        public Flight @Flight { get => _flight; }
        public Custommer @Custommer { get => _custommer; }
        public int Chair { get => _chair; }

        /// <summary>
        /// Hàm tạo vé
        /// </summary>
        /// <param name="flightId">id chuyến bay</param>
        /// <param name="custommerId">id khách hàng</param>
        /// <param name="chair">ghế</param>
        /// <param name="id">id vé</param>
        /// <exception cref="Exception">các lỗi sai về dữ liệu</exception>
        public Ticket(int flightId = 0, int custommerId = 0, int chair = 0, string id = null)
        {
            Flight flight = Flight.FindById(flightId);
            if (flight == null)
            {
                throw new Exception("Khong tim thay chuyen bay");
            }

            Custommer custommer = Custommer.FindWaitById(custommerId);
            if (custommer == null)
            {
                throw new Exception("Khong tim thay khach hang");
            }

            if (chair <1 && !flight.CheckedChair(chair))
            {
                throw new Exception("khong tim thay ghe");
            }

            if(!Custommer.RemoveCustomerWaitId(custommerId))
            {
                throw new Exception("khach hang khong co trong hang cho");
            }

            if (_ticketsList == null)
            {
                _ticketsList = new List<Ticket>();
            }


            this._flight = flight;
            this._custommer = custommer;
            this._chair = chair;
            this._id = id == null ? $"{this._flight.Id}{this._chair}" : id;
            this._flight.ChairsList.Remove(this._chair);
            this._flight.CustommersList.Add(this._custommer);
            Custommer.RemoveCustomerWaitId(custommerId);

            if (this._flight.ChairsList.Count == 0)
            {
                this._flight.Status = 2;
            }

            SaveFile();
            this._flight.TicketList.Add(this);
            _ticketsList.Add(this);
        }

        /// <summary>
        /// tìm kiếm vé theo id
        /// </summary>
        /// <param name="id">id vé</param>
        /// <returns>vé hoặc null</returns>
        public static Ticket FindById(string id)
        {
            Ticket ticket = null;

            if (_ticketsList != null)
            {
                foreach (Ticket ticketItem in _ticketsList)
                {
                    if (ticketItem.Id == id)
                    {
                        ticket = ticketItem;
                        break;
                    }
                }
            }

            return ticket;
        }

        /// <summary>
        /// trả vé
        /// </summary>
        /// <param name="id">id vé</param>
        /// <returns>thành công || thất bại</returns>
        public static bool ReturnTicketById(string id)
        {
            bool result = false;
            Ticket ticket = FindById(id);
            if (ticket != null)
            {
                if (ticket._flight.Status == 3)
                {
                    ticket.Flight.ChairsList.Add(ticket.Chair);
                    if (ticket.Flight.Status == 2)
                    {
                        ticket.Flight.Status = 1;
                    }
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// định dạng chuỗi lưu vào file
        /// </summary>
        /// <returns>mảng dữ liệu lưu file</returns>
        public static string[] ToFile()
        {
            List<string> strs = new List<string>();
            if (_ticketsList != null)
            {
                foreach (Ticket ticketItem in _ticketsList)
                {
                    strs.Add($"{ticketItem.Id}-{ticketItem._flight.Id}-{ticketItem._custommer.Id}-{ticketItem._chair}");
                }
            }
            return strs.ToArray();
        }

        /// <summary>
        /// đọc dữ liệu file
        /// </summary>
        public static void GetObjectsFromFile()
        {
            List<string> ticketsListData = IOFIle.ReadFile(FILE).ToList();
            if (ticketsListData.Count > 0)
            {
                for (int i = 0; i < ticketsListData.Count; i++)
                {
                        string[] ticketData = ticketsListData[i].Split("-");
                        try
                        {
                            _ = new Ticket(id: ticketData[0], flightId: int.Parse(ticketData[1]), custommerId: int.Parse(ticketData[2]), chair: int.Parse(ticketData[3]));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                }
            }
        }

        /// <summary>
        /// lưu dữ liệu vào file
        /// </summary>
        public static void SaveObjectsToFile()
        {
            if (!Directory.Exists("DB"))
            {
                Directory.CreateDirectory("DB");
            }
            IOFIle.WriteFile(FILE, ToFile());
        }

        /// <summary>
        /// lưu vé vào file {id}.txt
        /// </summary>
        public void SaveFile()
        {
            List<string> strs = new List<string>() { this.ToString()};
            if (!Directory.Exists("Ticket"))
            {
                Directory.CreateDirectory("Ticket");
            }
            IOFIle.WriteFile($"Ticket/{this._id}.txt", strs.ToArray());
        }

        public override string ToString() => $"Mã vé: {this._id}\tMã chuyến bay: {this._flight.Id}\tThông tin khách hàng: {this._custommer}\tSố thứ tự của ghế: {this._chair}";

    }
}
