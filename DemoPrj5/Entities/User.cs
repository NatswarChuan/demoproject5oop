﻿/**
 * Nguoi dung
 * */
using DemoPrj5.Interfaces;
using DemoPrj5.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Entities
{
    public class User : IUser
    {
        const string FILE = "DB/UsersList.txt";
        private static List<User> _usersList;
        public static int _autoIncrement = 0;

        private int _id;
        private string _name;
        private string _password;

        public int Id { get => _id; }
        public string Name { get => _name; }
        public string Password { get => _password; set => _password = value; }
        public static List<User> UsersList { get => _usersList; }

        /// <summary>
        /// hàm tạo tài khoản
        /// </summary>
        /// <param name="name">tên tài khoản</param>
        /// <param name="password">mật khẩu tài khoản</param>
        /// <param name="id">id tài khoản</param>
        /// <exception cref="Exception">lỗi dữ liệu</exception>
        public User(string name = "", string password = "", int? id = null)
        {
            if (name == null)
            {
                throw new Exception("ten trong");
            }
            if (password == null)
            {
                throw new Exception("mat khau trong");
            }
            if (_usersList != null && FindByName(name) != null)
            {
                throw new Exception("tai khoan da ton tai");
            }
            if (_usersList == null)
            {
                _usersList = new List<User>();
            }

            this._name = name;
            this._password = password;
            this._id = id.HasValue ? id.Value : ++_autoIncrement;

            _usersList.Add(this);
        }

        /// <summary>
        /// tìm kiếm tài khoản theo tên
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static User FindByName(string name)
        {
            User user = null;

            foreach (User userItem in _usersList)
            {
                if (userItem.Name == name)
                {
                    user = userItem;
                    break;
                }
            }

            return user;
        }

        /// <summary>
        /// định dạng chuỗi lưu vào file
        /// </summary>
        /// <returns>mảng dữ liệu lưu file</returns>
        public static string[] ToFile()
        {
            List<string> strs = new List<string>();
            strs.Add($"{_autoIncrement}");
            if (_usersList != null)
            {
                foreach (User userItem in _usersList)
                {
                    strs.Add($"{userItem.Id}-{userItem.Name}-{userItem.Password}");
                }
            }
            return strs.ToArray();
        }

        /// <summary>
        /// đọc dữ liệu file
        /// </summary>
        public static void GetObjectsFromFile()
        {
            List<string> usersListData = IOFIle.ReadFile(FILE).ToList();
            if (usersListData.Count > 0)
            {
                for (int i = 0; i < usersListData.Count; i++)
                {
                    if (i == 0)
                    {
                        _autoIncrement = int.Parse(usersListData[i]);
                    }
                    else
                    {
                        string[] userData = usersListData[i].Split("-");
                        try
                        {
                            _ = new User(id: int.Parse(userData[0]), name: userData[1], password: userData[2]);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// lưu dữ liệu vào file
        /// </summary>
        public static void SaveObjectsToFile()
        {
            if (!Directory.Exists("DB"))
            {
                Directory.CreateDirectory("DB");
            }
            IOFIle.WriteFile(FILE, ToFile());
        }

        /// <summary>
        /// kiểm tra mật khẩu
        /// </summary>
        /// <param name="password">mật khẩu</param>
        /// <returns>đúng || sai</returns>
        public bool CheckPassword(string password)
        {
            bool result = password == this._password;
            return result;
        }

        public override string ToString() => $"User:\tId: {this.Id}\tName: {this.Name}\tPassword: {this.Password}";
    }
}
