﻿/**
 * Khach hang
 * */
using DemoPrj5.Interfaces;
using DemoPrj5.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Entities
{
    public class Custommer
    {
        const string FILE = "DB/CustommersList.txt";
        private static List<Custommer> _custommerList;
        private static List<Custommer> _custommerWaitList;
        private static int _autoIncrement;

        private int _id;
        private string _identityCard;
        private string _name;
        
        public static List<Custommer> CustommerList { get => _custommerList;}
        public int Id { get => _id;}
        public string IdentityCard { get => _identityCard;}
        public string Name { get => _name;}

        /// <summary>
        /// hàm tạo khách hàng
        /// </summary>
        /// <param name="identityCard">số CMND</param>
        /// <param name="name">tên khách hàng</param>
        /// <param name="id">id khách hàng</param>
        public Custommer(string identityCard = "",string name = "", int? id = null)
        {
            if (_custommerList == null)
            {
                _custommerList = new List<Custommer>();
            }
            if (_custommerWaitList == null)
            {
                _custommerWaitList = new List<Custommer>();
            }
            this._id = id.HasValue ? id.Value : ++_autoIncrement;
            this._identityCard = identityCard;
            this._name = name;
            _custommerList.Add(this);
            _custommerWaitList.Add(this);
        }

        /// <summary>
        /// tìm khách hàng theo id
        /// </summary>
        /// <param name="id">id khách hàng</param>
        /// <returns>khách hàng || null</returns>
        public static Custommer FindById(int id)
        {
            Custommer custommer = null;

            if (_custommerList != null)
            {
                foreach (Custommer custommerItem in _custommerList)
                {
                    if (custommerItem.Id == id)
                    {
                        custommer = custommerItem;
                        break;
                    }
                }
            }

            return custommer;
        }

        /// <summary>
        /// tìm khách hàng chờ theo id
        /// </summary>
        /// <param name="id">id khách hàng</param>
        /// <returns>khách hàng || null</returns>
        public static Custommer FindWaitById(int id)
        {
            Custommer custommer = null;

            if (_custommerList != null)
            {
                foreach (Custommer custommerItem in _custommerWaitList)
                {
                    if (custommerItem.Id == id)
                    {
                        custommer = custommerItem;
                        break;
                    }
                }
            }

            return custommer;
        }

        /// <summary>
        /// xóa khách hàng khỏi danh sách chờ
        /// </summary>
        /// <param name="id">id khách hàng</param>
        /// <returns>thành công || thất bại</returns>
        public static bool RemoveCustomerWaitId(int id)
        {
            Custommer custommer = FindById(id);
            return _custommerWaitList.Remove(custommer);
        }

        /// <summary>
        /// định dạng chuỗi lưu vào file
        /// </summary>
        /// <returns>mảng dữ liệu lưu file</returns>
        public static string[] ToFile()
        {
            List<string> strs = new List<string>();
            strs.Add($"{_autoIncrement}");
            if (_custommerList != null)
            {
                foreach (Custommer custommerItem in _custommerList)
                {
                    strs.Add(custommerItem.ToStringFile());
                }
            }
            return strs.ToArray();
        }

        /// <summary>
        /// đọc dữ liệu file
        /// </summary>
        public static void GetObjectsFromFile()
        {
            List<string> custommersListData
                = IOFIle.ReadFile(FILE).ToList();
            if (custommersListData.Count > 0)
            {
                for (int i = 0; i < custommersListData.Count; i++)
                {
                    if (i == 0)
                    {
                        _autoIncrement = int.Parse(custommersListData[i]);
                    }
                    else
                    {
                        string[] custommerData = custommersListData[i].Split("-");
                        try
                        {
                            _ = new Custommer(id: int.Parse(custommerData[0]), name: custommerData[1], identityCard: custommerData[2]);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// lưu dữ liệu vào file
        /// </summary>
        public static void SaveObjectsToFile()
        {
            if (!Directory.Exists("DB"))
            {
                Directory.CreateDirectory("DB");
            }
            IOFIle.WriteFile(FILE, ToFile());
        }

        /// <summary>
        /// định dạng chuỗi luôi file
        /// </summary>
        /// <returns>chuỗi lưu file</returns>
        public string ToStringFile() => $"{this.Id}-{this.Name}-{this.IdentityCard}";

        public override string ToString() => $"Họ tên: {this.Name}\tCMND: {this.IdentityCard}";
    }
}
