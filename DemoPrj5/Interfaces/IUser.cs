﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Interfaces
{
    public interface IUser
    {
        public bool CheckPassword(string password);
    }
}
