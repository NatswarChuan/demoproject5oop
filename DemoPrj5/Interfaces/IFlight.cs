﻿using DemoPrj5.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrj5.Interfaces
{
    public interface IFlight
    {
        public List<int> GenChairsList();
        public string TicketListToString();
        public void ShowEmptyChairList();
        public void ShowCustommerList();
        public List<Custommer> FindCustommerByName(string name);
        public bool CheckedChair(int chair);
    }
}
